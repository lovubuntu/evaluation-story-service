import { Request, Response } from 'express';
import UsersController from '../../../src/Components/User/UsersController';
import UsersService from '../../../src/Components/User/UsersService';

describe('UsersController', () => {
  let usersService: UsersService;
  let request: Request;
  let response: Response;
  const newUser = {
    name: 'name',
    email: 'email@something.com',
    password: 'password',
    role: 'ADMIN',
  };

  const createController = () => new UsersController(usersService);

  beforeEach(() => {
    usersService = {} as jest.Mocked<UsersService>;
    usersService.saveUser = jest.fn().mockResolvedValue(newUser);

    request = {} as jest.Mocked<Request>;

    request.body = newUser;
    response = {} as jest.Mocked<Response>;

    response.json = jest.fn();
  });

  it('should create a new user from the request', async () => {
    delete newUser.password;

    await createController().create(request, response);

    expect(usersService.saveUser).toBeCalledWith(newUser);
    expect(response.json).toBeCalledWith({user: newUser});
  });
});
