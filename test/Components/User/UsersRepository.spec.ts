import { Connection } from 'typeorm';
import { User } from '../../../src/Components/User/User';
import { UserCreateRequestParams } from '../../../src/Components/User/UserCreateRequestParams';
import { UserRole } from '../../../src/Components/User/UserRole';
import UsersRepository from '../../../src/Components/User/UsersRepository';

describe('UsersRepository', () => {
  let userDetails: UserCreateRequestParams;
  const userEmail = 'user@example.com';
  let connection: Connection;
  let user: User;

  const createRepository = () => new UsersRepository(connection);

  beforeEach(() => {
    connection = {} as jest.Mocked<Connection>;

    user = new User();
    user.name = 'some name';
    user.email = 'email@here.com';

    userDetails = {
      name: 'Sample User',
      email: userEmail,
      password: 'password',
      role: UserRole.MEMBER,
    };

    // @ts-ignore
    connection.manager = {
      create: jest.fn().mockReturnValue(userDetails),
      save: jest.fn(),
      findOne: jest.fn(),
    };
  });

  it('should create a user with the given details', async () => {
    await createRepository().createUser(userDetails);

    expect(connection.manager.create).toBeCalledWith(User, userDetails);
    expect(connection.manager.save).toBeCalledWith(userDetails);
  });

  it('should get an existing user using their email', async () => {
    const sampleUser = {email: userEmail, name: 'name', id: 1};
    connection.manager.findOne = jest.fn().mockReturnValue(sampleUser);

    const existingUser = await createRepository().getUserByEmail(userEmail);

    expect(connection.manager.findOne).toBeCalledWith(User, {email: userEmail});
    expect(existingUser).toEqual(sampleUser);
  });
});
