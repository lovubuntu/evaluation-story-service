import { UserCreateRequestParams } from '../../../src/Components/User/UserCreateRequestParams';
import { UserRole } from '../../../src/Components/User/UserRole';
import UsersRepository from '../../../src/Components/User/UsersRepository';
import UsersService from '../../../src/Components/User/UsersService';

describe('UsersService', () => {
  let userRepo: UsersRepository;
  let newUserParams: UserCreateRequestParams;
  const userEmail = 'user@example.com';

  const createService = () => new UsersService(userRepo);

  beforeEach(() => {
    userRepo = {} as jest.Mocked<UsersRepository>;
    userRepo.createUser = jest.fn();
    userRepo.getUserByEmail = jest.fn();

    newUserParams = {
      name: 'Sample User',
      email: userEmail,
      password: 'password',
      role: UserRole.MEMBER,
    };
  });

  it('should create a normal user', async () => {
    await createService().saveUser(newUserParams);

    expect(userRepo.createUser).toBeCalledWith({
      email: userEmail,
      name: 'Sample User',
      password: expect.any(String),
      role: UserRole.MEMBER,
    });
  });

  it('should create an admin user', async () => {
    newUserParams.role = UserRole.ADMIN;

    await createService().saveUser(newUserParams);

    expect(userRepo.createUser).toBeCalledWith({
      email: userEmail,
      name: 'Sample User',
      password: expect.any(String),
      role: UserRole.ADMIN,
    });
  });

  it('should get an existing user when correct email is present', async () => {
    const mockUser = {
      id: 1,
      name: 'Name',
      email: userEmail,
      role: UserRole.MEMBER,
    };
    userRepo.getUserByEmail = jest.fn().mockResolvedValue(mockUser);

    const user = await createService().getUserByEmail(userEmail);

    expect(userRepo.getUserByEmail).toBeCalledWith(userEmail);
    expect(user).toEqual(mockUser);
  });
});
