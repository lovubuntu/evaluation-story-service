import { Application } from 'express';
import UsersController from '../../../src/Components/User/UsersController';
import UsersRoutes from '../../../src/Components/User/UsersRoutes';

describe('UsersRoutes', () => {
  let usersController: UsersController;
  let app: Application;

  const createRoutes = () => new UsersRoutes(usersController);

  beforeEach(() => {
    usersController = {} as jest.Mocked<UsersController>;
    usersController.create = jest.fn();

    app = {} as jest.Mocked<Application>;
    app.route = jest.fn().mockReturnThis();
    app.post = jest.fn();

  });

  it('should add a new route to create user', async () => {
    await createRoutes().routes(app);

    expect(app.route).toBeCalledWith('/users');
    expect(app.post).toBeCalled();
  });
});
