import { Application } from 'express';
import { SchemaValidationMiddleware } from '../../../src/API/SchemaValidationMiddleware';
import { StoriesController } from '../../../src/Components/Story/StoriesController';
import { StoriesRoutes } from '../../../src/Components/Story/StoriesRoutes';

describe('StoriesRoutes', () => {
  let storiesController: StoriesController;
  let schemaValidationMiddleware: SchemaValidationMiddleware;
  let app: Application;

  const createRoutes = () => new StoriesRoutes(storiesController, schemaValidationMiddleware);

  beforeEach(() => {
    storiesController = {} as jest.Mocked<StoriesController>;
    storiesController.handleCreate = jest.fn();
    storiesController.getUserStories = jest.fn();
    storiesController.handleReview = jest.fn();

    schemaValidationMiddleware = {} as jest.Mocked<SchemaValidationMiddleware>;
    schemaValidationMiddleware.execute = jest.fn();

    app = {} as jest.Mocked<Application>;
    app.route = jest.fn().mockReturnThis();
    app.post = jest.fn();
    app.get = jest.fn();
    app.put = jest.fn();

  });

  it('should add routes for stories', async () => {
    await createRoutes().routes(app);

    expect(app.route).toBeCalledWith('/stories');
    expect(app.route).toBeCalledWith('/stories/:id/review');

    expect(app.post).toBeCalled();
    expect(app.get).toBeCalled();
    expect(app.put).toBeCalled();
  });
});
