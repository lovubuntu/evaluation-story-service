import { Connection } from 'typeorm';
import { NotFoundError } from '../../../src/API/errors/NotFoundError';
import { CreateStoryParams } from '../../../src/Components/Story/CreateStoryParams';
import { StoriesRepository } from '../../../src/Components/Story/StoriesRepository';
import { Story } from '../../../src/Components/Story/Story';
import { StoryComplexity } from '../../../src/Components/Story/StoryComplexity';
import { StoryStatus } from '../../../src/Components/Story/StoryStatus';
import { StoryType } from '../../../src/Components/Story/StoryType';

describe('StoriesRepository', () => {
  let connection: Connection;
  let storyDetails: CreateStoryParams;
  let createdUserStory: Story;

  const createRepository = () => new StoriesRepository(connection);

  beforeEach(() => {
    connection = {} as jest.Mocked<Connection>;

    storyDetails = {
      summary: 'summary',
      description: 'description',
      estimatedDaysToComplete: 2.5,
      type: StoryType.BUGFIX,
      complexity: StoryComplexity.EXTRA_LARGE,
      cost: 5,
      userId: 1,
    };

    createdUserStory = {...storyDetails, id: 1, status: StoryStatus.REQUEST_FOR_APPROVAL} as Story;

    // @ts-ignore
    connection.manager = {
      create: jest.fn().mockReturnValue(storyDetails),
      save: jest.fn(),
      update: jest.fn().mockReturnValue({affected: 1}),
      find: jest.fn().mockReturnValue([createdUserStory]),
    };
  });

  it('should create a new story with the given details', async () => {
    await createRepository().createStory(storyDetails);

    expect(connection.manager.create).toBeCalledWith(Story, storyDetails);
    expect(connection.manager.save).toBeCalledWith(storyDetails);
  });

  it('should return stories created by a single user', async () => {
    const ownUserStories = await createRepository().getStoriesByUser(1);

    expect(connection.manager.find).toBeCalledWith(Story, {where: {userId: 1}});
    expect(ownUserStories).toEqual([createdUserStory]);
  });

  it('should return all the user stories irrespective of the author', async () => {
    const allUserStories = await createRepository().getAllUserStories();

    expect(connection.manager.find).toBeCalledWith(Story);
    expect(allUserStories).toEqual([createdUserStory]);
  });

  it('should update the status of a user story', async () => {
    await createRepository().updateStatus(createdUserStory.id, StoryStatus.APPROVED);

    expect(connection.manager.update).toBeCalledWith(
      Story,
      {id: createdUserStory.id},
      {status: StoryStatus.APPROVED});
  });

  it('should throw error when an invalid story id is given', async () => {
    connection.manager.update = jest.fn().mockReturnValue({affected: 0});

    const repo = createRepository();
    await expect(repo.updateStatus(543, StoryStatus.APPROVED)).rejects.toThrow(NotFoundError);
  });
});
