import { ForbiddenError } from '../../../src/API/errors/ForbiddenError';
import { CreateStoryParams } from '../../../src/Components/Story/CreateStoryParams';
import { StoriesRepository } from '../../../src/Components/Story/StoriesRepository';
import { StoriesService } from '../../../src/Components/Story/StoriesService';
import { Story } from '../../../src/Components/Story/Story';
import { StoryComplexity } from '../../../src/Components/Story/StoryComplexity';
import { StoryStatus } from '../../../src/Components/Story/StoryStatus';
import { StoryType } from '../../../src/Components/Story/StoryType';
import { UserResponseType } from '../../../src/Components/User/UserResponseType';
import { UserRole } from '../../../src/Components/User/UserRole';

describe('StoriesService', () => {
  let storiesRepository: StoriesRepository;
  let storyParams: CreateStoryParams;
  let user: UserResponseType;
  let createdUserStory: Story;

  const createService = () => new StoriesService(storiesRepository);

  beforeEach(() => {
    user = {
      id: 1,
      name: 'User Name',
      email: 'good@user.com',
      role: UserRole.MEMBER,
    };

    storyParams = {
      summary: 'summary',
      description: 'description',
      estimatedDaysToComplete: 2.5,
      type: StoryType.BUGFIX,
      complexity: StoryComplexity.EXTRA_LARGE,
      cost: 5,
    };

    createdUserStory = {...storyParams, id: 1, userId: 1, status: StoryStatus.REQUEST_FOR_APPROVAL} as Story;

    storiesRepository = {} as jest.Mocked<StoriesRepository>;
    storiesRepository.createStory = jest.fn().mockReturnValue(createdUserStory);
    storiesRepository.getStoriesByUser = jest.fn().mockReturnValue([createdUserStory]);
    storiesRepository.getAllUserStories = jest.fn().mockReturnValue([createdUserStory]);
    storiesRepository.updateStatus = jest.fn();
  });

  it('should save a new user story with user information', async () => {
    const story = await createService().createUserStory(storyParams, user);

    expect(storiesRepository.createStory).toHaveBeenCalledWith({...storyParams, userId: user.id});
    expect(story).toEqual(createdUserStory);
  });

  it('should return own stories of that particular user when the user is a member', async () => {
    const storiesCreatedByTheUser = await createService().getStories(user);

    expect(storiesRepository.getStoriesByUser).toHaveBeenCalledWith(user.id);
    expect(storiesRepository.getAllUserStories).not.toHaveBeenCalled();
    expect(storiesCreatedByTheUser).toEqual([createdUserStory]);
  });

  it('should return stories of all users when the user is an admin', async () => {
    user.role = UserRole.ADMIN;

    const allUserStories = await createService().getStories(user);

    expect(storiesRepository.getAllUserStories).toHaveBeenCalled();
    expect(allUserStories).toEqual([createdUserStory]);
    expect(storiesRepository.getStoriesByUser).not.toHaveBeenCalled();
  });

  it('should be able to review story when the user is an admin', async () => {
    user.role = UserRole.ADMIN;

    await createService().review(1, StoryStatus.APPROVED, user);

    expect(storiesRepository.updateStatus).toHaveBeenCalledWith(1, StoryStatus.APPROVED);
  });

  it('should not allow normal users to review a story', async () => {
    await expect(createService().review(1, StoryStatus.APPROVED, user))
      .rejects.toThrow(ForbiddenError);
  });
});
