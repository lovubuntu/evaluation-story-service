import { Request, Response } from 'express';
import { CreateStoryParams } from '../../../src/Components/Story/CreateStoryParams';
import { StoriesController } from '../../../src/Components/Story/StoriesController';
import { StoriesService } from '../../../src/Components/Story/StoriesService';
import { Story } from '../../../src/Components/Story/Story';
import { StoryComplexity } from '../../../src/Components/Story/StoryComplexity';
import { StoryStatus } from '../../../src/Components/Story/StoryStatus';
import { StoryType } from '../../../src/Components/Story/StoryType';
import { UserResponseType } from '../../../src/Components/User/UserResponseType';
import { UserRole } from '../../../src/Components/User/UserRole';

describe('StoriesController', () => {
  let storiesService: StoriesService;
  const createController = () => new StoriesController(storiesService);
  let request: Request;
  let response: Response;
  let storyParams: CreateStoryParams;
  let createdUserStory: Story;
  let user: UserResponseType;

  beforeEach(() => {
    user = {
      id: 1,
      name: 'User Name',
      email: 'good@user.com',
      role: UserRole.MEMBER,
    };

    storyParams = {
      summary: 'summary',
      description: 'description',
      estimatedDaysToComplete: 2.5,
      type: StoryType.BUGFIX,
      complexity: StoryComplexity.EXTRA_LARGE,
      cost: 5,
    };

    request = {} as jest.Mocked<Request>;
    request.body = storyParams;
    request.params = {id: '1'};

    response = {} as jest.Mocked<Response>;
    response.status = jest.fn().mockReturnThis();
    response.locals = {user};
    response.json = jest.fn();

    createdUserStory = {...storyParams, id: 1, status: StoryStatus.REQUEST_FOR_APPROVAL} as Story;

    storiesService = {} as jest.Mocked<StoriesService>;
    storiesService.createUserStory = jest.fn().mockReturnValue(createdUserStory);
    storiesService.getStories = jest.fn().mockReturnValue([createdUserStory]);
    storiesService.review = jest.fn();
  });

  it('should create a new story for a user', async () => {
    await createController().handleCreate(request, response);

    expect(storiesService.createUserStory).toHaveBeenCalledWith(storyParams, user);
    expect(response.json).toHaveBeenCalledWith(createdUserStory);
    expect(response.status).toHaveBeenCalledWith(201);
  });

  it('should throw exception when not able to create a user story', async () => {
    storiesService.createUserStory = jest.fn().mockImplementation(() => {
      throw new Error('Something happened');
    });

    await expect(createController().handleCreate(request, response)).rejects.toThrow();
  });

  it('should return all stories', async () => {
    await createController().getUserStories(request, response);

    expect(storiesService.getStories).toHaveBeenCalledWith(user);
    expect(response.json).toHaveBeenCalledWith({stories: [createdUserStory]});
  });

  it('should review a given story', async () => {
    request.body = {status: StoryStatus.APPROVED};

    await createController().handleReview(request, response);

    expect(storiesService.review).toHaveBeenCalledWith(1, StoryStatus.APPROVED, user);
    expect(response.status).toHaveBeenCalledWith(204);
    expect(response.json).toHaveBeenCalled();
  });

  it('should throw exception when not able to review a story', async () => {
    storiesService.review = jest.fn().mockImplementation(() => {
      throw new Error('Something happened');
    });

    await expect(createController().handleReview(request, response)).rejects.toThrow();
  });
});
