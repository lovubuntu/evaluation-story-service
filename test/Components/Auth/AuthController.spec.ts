import { Request, Response } from 'express';
import { UnauthorizedError } from '../../../src/API/errors/UnauthorizedError';
import { AuthController } from '../../../src/Components/Auth/AuthController';
import { AuthService } from '../../../src/Components/Auth/AuthService';
import { UserResponseType } from '../../../src/Components/User/UserResponseType';
import { UserRole } from '../../../src/Components/User/UserRole';

describe('AuthController', () => {
  let authService: AuthService;
  const createController = () => new AuthController(authService);
  let request: Request;
  let response: Response;
  const userLogin = {
    email: 'good@user.com',
    password: 'validpassword',
  };
  let user: UserResponseType;
  let token: string;

  beforeEach(() => {
    request = {} as jest.Mocked<Request>;
    request.body = userLogin;

    response = {} as jest.Mocked<Response>;
    response.json = jest.fn();

    user = {
      id: 1,
      name: 'User Name',
      email: 'good@user.com',
      role: UserRole.MEMBER,
    };
    token = 'jwt token';

    authService = {} as jest.Mocked<AuthService>;
    authService.login = jest.fn().mockResolvedValue({user, token});
  });

  it('should allow valid user to login', async () => {
    await createController().handleLogin(request, response);

    expect(authService.login).toHaveBeenCalledWith(userLogin.email, userLogin.password);
    expect(response.json).toHaveBeenCalledWith({user, token});
  });

  it('should throw exception for an invalid user', async () => {
    authService.login = jest.fn().mockImplementation((_request, _response) => {
      throw new UnauthorizedError('Invalid email/password');
    });

    await expect(createController().handleLogin(request, response))
      .rejects.toThrow(UnauthorizedError);
  });
});
