import { Application } from 'express';
import { AuthController } from '../../../src/Components/Auth/AuthController';
import AuthRoutes from '../../../src/Components/Auth/AuthRoutes';
import { SchemaValidationMiddleware } from "../../../src/API/SchemaValidationMiddleware";

describe('AuthRoutes', () => {
  let authController: AuthController;
  let schemaValidator: SchemaValidationMiddleware;
  let app: Application;

  const createRoutes = () => new AuthRoutes(authController, schemaValidator);

  beforeEach(() => {
    authController = {} as jest.Mocked<AuthController>;
    authController.handleLogin = jest.fn();

    schemaValidator = {} as jest.Mocked<SchemaValidationMiddleware>;
    schemaValidator.execute = jest.fn();

    app = {} as jest.Mocked<Application>;
    app.route = jest.fn().mockReturnThis();
    app.post = jest.fn();

  });

  it('should add a route to validate users login', async () => {
    await createRoutes().routes(app);

    expect(app.route).toBeCalledWith('/auth/login');
    expect(app.post).toBeCalled();
  });
});
