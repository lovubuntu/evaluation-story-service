import { UnauthorizedError } from '../../../src/API/errors/UnauthorizedError';
import AuthService from '../../../src/Components/Auth/AuthService';
import TokenManager from '../../../src/Components/Auth/TokenManager';
import { UserResponseType } from '../../../src/Components/User/UserResponseType';
import { UserRole } from '../../../src/Components/User/UserRole';
import UsersService from '../../../src/Components/User/UsersService';

describe('AuthService', () => {
  let userService: UsersService;
  let tokenManager: TokenManager;
  let user: UserResponseType;
  const userEmail = 'valid@user.com';

  const createService = () => new AuthService(userService, tokenManager);

  beforeEach(() => {
    user = {
      email: userEmail,
      id: 1,
      name: 'User name',
      role: UserRole.MEMBER,
    };

    userService = {} as jest.Mocked<UsersService>;
    userService.getUserByEmail = jest.fn().mockReturnValue({
      ...user,
      password: '$2b$10$/lz/Pg7KlNsWoQhKihX9dO9H1yWXWomjk3M5fic3b5Dv.HoWfAYv.',
    });

    tokenManager = {} as TokenManager;
    tokenManager.generateToken = jest.fn().mockReturnValue('token');

  });

  it('should return user info and jwt token for a valid user', async () => {
    const userResponse = await createService().login(userEmail, 'password');

    expect(userResponse).toEqual({user, token: 'token'});
  });

  it('should throw error when the user email does not exist', async () => {
    userService.getUserByEmail = jest.fn();

    await expect(createService().login(userEmail, 'password'))
      .rejects.toThrow(UnauthorizedError);
  });

  it('should throw error when there is a password mismatch', async () => {
    await expect(createService().login(userEmail, 'bad password'))
      .rejects.toThrow(UnauthorizedError);
  });
});
