// @ts-ignore
import { sign } from 'jsonwebtoken';
import TokenManager from '../../../src/Components/Auth/TokenManager';
import { UserResponseType } from '../../../src/Components/User/UserResponseType';
import { UserRole } from '../../../src/Components/User/UserRole';
import { LoggerInterface } from '../../../src/Infra/LoggerInterface';

describe('TokenManager', () => {
  let logger: LoggerInterface;
  let user: UserResponseType;
  const userEmail = 'valid@user.com';
  const signMock = jest.fn();

  const createTokenManager = () => new TokenManager('Secret', logger);

  beforeEach(() => {
    // @ts-ignore
    sign = signMock.mockReturnValue('jwt token');
    logger = {} as jest.Mocked<LoggerInterface>;
    logger.info = jest.fn();
    logger.debug = jest.fn();

    user = {
      email: userEmail,
      id: 1,
      name: 'User name',
      role: UserRole.MEMBER,
    };
  });

  it('should generate a jwt token for the given user details', async () => {
    const token = await createTokenManager().generateToken(user);

    expect(signMock).toBeCalledWith({user}, 'Secret');
    expect(token).toEqual('jwt token');
  });

  it('should authenticate a user when the token is valid', async () => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoidmFsaWRAdXNlci5jb20iLCJpZCI6MSwibmFt' +
      'ZSI6IlVzZXIgbmFtZSIsInJvbGUiOiJNRU1CRVIifSwiaWF0IjoxNTg2MTEwMzA0fQ.q2H0KBM0P1Aw9E_5460vRSwQn5mPcGhQjqFrvZXUToY';
    const jwtUser = await createTokenManager()
      .authenticateUser(token);

    expect(jwtUser).toEqual(user);
  });

  it('should throw exception when it is a malformed token', async () => {
    await expect(createTokenManager().authenticateUser('malformed token'))
      .rejects.toThrow();
  });
});
