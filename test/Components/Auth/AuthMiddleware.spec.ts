import { Request, Response } from 'express';

import { UnauthorizedError } from '../../../src/API/errors/UnauthorizedError';
import { AuthMiddleware } from '../../../src/Components/Auth/AuthMiddleware';
import TokenManager from '../../../src/Components/Auth/TokenManager';

describe('AuthMiddleware', () => {
  let tokenManager: TokenManager;
  let request: Request;
  let response: Response;
  let next: () => void;

  const createMiddleware = () => new AuthMiddleware(tokenManager);

  beforeEach(() => {
    tokenManager = {} as jest.Mocked<TokenManager>;
    tokenManager.authenticateUser = jest.fn().mockReturnValue({
      email: 'test@test.com',
    });

    request = {} as jest.Mocked<Request>;
    request.headers = {
      authorization: 'Bearer: 123123',
    };

    response = {} as jest.Mocked<Response>;
    response.json = jest.fn();
    response.send = jest.fn();
    response.status = jest.fn().mockReturnThis();
    response.locals = {};

    next = jest.fn();
  });

  it('should throw error when token is not provided', async () => {
    request.headers = {};

    await expect(createMiddleware().execute(request, response, next)).rejects
      .toThrow(UnauthorizedError);
  });

  it('should throw error when token is invalid', async () => {
    tokenManager.authenticateUser = jest.fn().mockImplementation(() => {
      throw new Error('Not Right');
    });
    request.headers = {authorization: 'dummy token'};

    await expect(createMiddleware().execute(request, response, next)).rejects
      .toThrow(UnauthorizedError);
  });

  it('should throw error when user object is not found in a valid token', async () => {
    tokenManager.authenticateUser = jest.fn();
    const tokenWithoutUser = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImR1bW15Ijp7ImVtYWlsIjoidmFsaWRAdXNl' +
      'ci5jb20iLCJpZCI6MSwibmFtZSI6IlVzZXIgbmFtZSIsInJvbGUiOiJNRU1CRVIifX0sImlhdCI6MTU4NjExMjEwOH0.7dbKupJq34PN7g9y' +
      'Nhf7_DZoVe00fr4w_GjXm48tQ8I';
    request.headers = {
      authorization: tokenWithoutUser
    };

    await expect(createMiddleware().execute(request, response, next)).rejects
      .toThrow(UnauthorizedError);
  });

  it('should authenticate user with a valid token', async () => {
    await createMiddleware().execute(request, response, next);

    expect(response.locals.user).toEqual({
      email: 'test@test.com',
    });

    expect(next).toHaveBeenCalled();
  });
});
