import { Ajv } from 'ajv';
import { NextFunction, Request, Response } from 'express';

import { SchemaValidationMiddleware } from '../../src/API/SchemaValidationMiddleware';

describe('SchemaValidationMiddleware', () => {
  let validator: Ajv;
  let request: Request;
  let response: Response;
  let next: () => void;
  let middleware: (request: Request, response: Response, next: NextFunction) => void;

  const createValidatorMiddleware = () => new SchemaValidationMiddleware(validator);

  beforeEach(() => {
    validator = {} as jest.Mocked<Ajv>;
    validator.validate = jest.fn().mockReturnValue(true);

    request = {} as jest.Mocked<Request>;
    request.body = {
      login: 'test',
    };
    request.params = {
      login: 'test',
    };

    response = {} as jest.Mocked<Response>;
    response.send = jest.fn();
    response.status = jest.fn().mockReturnThis();

    next = jest.fn();

    middleware = createValidatorMiddleware().execute({body: {properties: {login: {type: 'string'}}}});
  });

  it('should return a middleware validator for given schema', () => {
    expect(createValidatorMiddleware().execute({})).toBeInstanceOf(Function);
  });

  it('should validate against schema', () => {
    middleware(request, response, next);

    expect(next).toHaveBeenCalled();
  });

  it('should throw error for invalid body data when validating body schema', () => {
    validator = {} as jest.Mocked<Ajv>;
    validator.validate = jest.fn().mockReturnValue(false);
    validator.errorsText = jest.fn().mockReturnValue('data should have required property "login"');
    middleware = createValidatorMiddleware().execute({body: {}});

    expect(() => middleware(request, response, next))
      .toThrowError('Missing mandatory fields: data should have required property "login"');
  });

  it('should throw error for invalid params data against params schema', async () => {
    validator = {} as jest.Mocked<Ajv>;
    validator.validate = jest.fn().mockReturnValue(false);
    validator.errorsText = jest.fn().mockReturnValue('data should have required property "storyId"'),
      middleware = createValidatorMiddleware().execute({params: {}});

    expect(() => middleware(request, response, next))
      .toThrowError('Missing mandatory fields: data should have required property "storyId"');
  });

  it('should return valid if schema do not have params or body', () => {
    middleware = createValidatorMiddleware().execute({});

    middleware(request, response, next);

    expect(next).toHaveBeenCalled();
    expect(validator.validate).not.toBeCalled();
  });
});
