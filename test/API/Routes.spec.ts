import { Application } from 'express';

import { Routes } from '../../src/API/Routes';
import { AuthMiddleware } from '../../src/Components/Auth/AuthMiddleware';
import AuthRoutes from '../../src/Components/Auth/AuthRoutes';
import SwaggerRoutes from '../../src/Components/Docs/SwaggerRoutes';
import { StoriesRoutes } from '../../src/Components/Story/StoriesRoutes';
import UsersRoutes from '../../src/Components/User/UsersRoutes';

describe('Routes', () => {
  let authMiddleware: AuthMiddleware;
  let swaggerRoutes: SwaggerRoutes;
  let usersRoutes: UsersRoutes;
  let authRoutes: AuthRoutes;
  let storiesRoutes: StoriesRoutes;
  let app: Application;

  const createRoutes = () => new Routes(
    swaggerRoutes,
    authMiddleware,
    authRoutes,
    usersRoutes,
    storiesRoutes,
  );

  beforeEach(() => {
    app = {} as jest.Mocked<Application>;
    app.route = jest.fn().mockReturnThis();
    app.use = jest.fn().mockReturnThis();

    authMiddleware = {} as jest.Mocked<AuthMiddleware>;
    authMiddleware.execute = jest.fn();

    authRoutes = {} as jest.Mocked<AuthRoutes>;
    authRoutes.routes = jest.fn();

    usersRoutes = {} as jest.Mocked<UsersRoutes>;
    usersRoutes.routes = jest.fn();

    storiesRoutes = {} as jest.Mocked<StoriesRoutes>;
    storiesRoutes.routes = jest.fn();

    swaggerRoutes = {} as jest.Mocked<SwaggerRoutes>;
    swaggerRoutes.routes = jest.fn();
  });

  it('should initialize all routes', () => {
    createRoutes().routes(app);

    expect(swaggerRoutes.routes).toHaveBeenCalled();
    expect(authRoutes.routes).toHaveBeenCalled();
    expect(usersRoutes.routes).toHaveBeenCalled();
    expect(storiesRoutes.routes).toHaveBeenCalled();
  });
});
