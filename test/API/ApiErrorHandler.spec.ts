import { Request, Response } from 'express';
import { Logger } from 'winston';

import { ApiErrorHandler } from '../../src/API/ApiErrorHandler';
import { ClientError } from '../../src/API/errors/ClientError';

describe('ApiErrorHandler', () => {
  let errorHandler: ApiErrorHandler;
  let logger: Logger;
  let envType: string;
  let response: Response;
  let request: Request;
  const clientError: ClientError = new ClientError();

  const nextFunction = jest.fn();
  const error = new Error('I am not programmed to handle this');

  const createHandler = () => new ApiErrorHandler(logger, envType);

  beforeEach(() => {
    logger = {} as jest.Mocked<Logger>;
    logger.error = jest.fn();
    logger.warn = jest.fn();

    request = {} as jest.Mocked<Request>;

    response = {} as jest.Mocked<Response>;
    response.get = jest.fn().mockReturnValue('UUID');
    response.json = jest.fn();
    response.status = jest.fn().mockReturnThis();

    envType = 'development';
    errorHandler = createHandler();
  });

  it('should handle client errors', async () => {
    errorHandler.handle(clientError, request, response, nextFunction);

    expect(logger.warn).toHaveBeenCalledWith({
      name: clientError.name,
      requestId: 'UUID',
      message: 'Bad Request',
      stack: expect.any(String),
    });
    expect(response.status).toHaveBeenCalledWith(400);
    expect(response.json).toHaveBeenCalledWith({message: 'Bad Request'});
  });

  it('should handle unexpected server errors', async () => {
    errorHandler.handle(error, request, response, nextFunction);

    expect(response.status).toHaveBeenCalledWith(500);
    expect(logger.error).toHaveBeenCalled();
  });

  describe('when in production environment', () => {
    beforeEach(() => {
      envType = 'production';
      errorHandler = createHandler();
    });

    it('should return error response without details', () => {
      errorHandler.handle(error, request, response, nextFunction);

      expect(logger.error).toHaveBeenCalledWith({
        requestId: 'UUID',
        message: 'I am not programmed to handle this',
        stack: expect.any(String),
      });
      expect(response.status).toHaveBeenCalledWith(500);
      expect(response.json).toHaveBeenCalledWith({message: 'Internal Server Error'});
    });
  });

  describe('when in any environment other than production', () => {
    beforeEach(() => {
      envType = 'development';
      errorHandler = createHandler();
    });

    it('should return detailed error response for server error', () => {
      errorHandler.handle(error, request, response, nextFunction);

      expect(response.status).toHaveBeenCalledWith(500);
      expect(logger.error).toHaveBeenCalledWith({
        requestId: 'UUID',
        message: 'I am not programmed to handle this',
        stack: expect.any(String),
      });
      expect(response.json).toHaveBeenCalledWith({
        message: 'I am not programmed to handle this',
        stack: expect.any(String),
      });
    });
  });
});
