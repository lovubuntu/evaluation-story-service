# Story Service
This is a service to create and review user stories.

I've tried using dependency injection technique as the primary mechanism to handle the tasks. This makes testing easier as the dependencies are all injected and not instantiated inside the objects. Also all the logic and actions related to a component are grouped inside its own directory.

## Requirements

- [nvm](https://github.com/creationix/nvm)
- [yarn](https://yarnpkg.com/lang/en/)

## Structure

- `API` - This is the layer in which the service is wired up and has some common functions that gets used across the application
- `Components` - Everything about a component will be under its own folder, like Controllers, services, routes and repositories
- `Infra` - This folder is supposed to have infrastructure components like Database, Cache etc.

## Validations

- Input request validations are done using [AJV](https://github.com/epoberezkin/ajv)

## Documentation

API Documentation is available under the route `<base-url>/api-docs` and follows Open API spec 3

## Assumptions

- User creation and maintenance are not the core of this problem. So exposing an endpoint to create new users (Admin/Member) for the ease of access. In actual application Admin shouldn't be created like this.

## Dependency Injection

This Project uses Dependency Injection. Every module has a pair of `DependencyInjection.ts` and `Types.ts` files. `Types.ts` file is used to name dependencies consistently and `DependencyInjection.ts` is used to instantiate and wire dependencies together.

## Bootstrap

- `src/envSchema.ts` - Define and group the type of environment variables here in order for them to be available in the application.
- `src/kernel.ts` - Bootstrap all the dependency injection container definitions in here so all the dependencies are wired properly. If a new module is defined along `DependencyInjection.ts` and `Types.ts`, it will have to be registered in this bootstrap.

## Install

```
yarn
```

## Build

```
cp .env.example .env
yarn build
```

## Run the project

```
// Add the db credentials to the .env file
yarn start
```

## Test

```
yarn test
```

### Coverage

```
yarn test-coverage
```
