const SnakeNamingStrategy = require('typeorm-naming-strategies').SnakeNamingStrategy;

module.exports = {
   type: "postgres",
   "namingStrategy": new SnakeNamingStrategy(),
   entities: [
      "src/Components/User/User{.ts,.js}",
      "src/Components/Story/Story{.ts,.js}"
   ],
   migrations: [
      "src/migration/**/*{.ts,.js}"
   ],
   cli: {
      migrationsDir: "src/migration",
   }
};
