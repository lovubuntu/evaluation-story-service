import { logger } from '../src/logger';

import { App } from '../src/API/App';
import { APITypes, boot } from '../src/kernel';

import { env } from '../src/env';

export async function startApi() {
  const container = await boot(env);

  const api: App = container.get(APITypes.App.toString());

  api.app.listen(env.infra.PORT, () => {
    logger.info(`Express server listening on port ${env.infra.PORT}`);
  });
}

startApi()
  .then(() => logger.info('started successfully'))
  .catch((err) => logger.error(err));
