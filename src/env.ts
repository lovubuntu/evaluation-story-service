import { loadFrom, loadFromEnv } from '@freighthub/typed-env';
import * as dotenv from 'dotenv';
import { readFileSync } from 'fs';
import * as path from 'path';

import { schema } from './envSchema';

let loadedEnv;
if (process.env.NODE_ENV === 'test') {
  const envConfig = dotenv.parse(readFileSync(path.resolve(__dirname, '../.env.test')));

  loadedEnv = loadFrom(envConfig, schema);
} else {
  dotenv.config();

  loadedEnv = loadFromEnv(schema);
}

export const env = loadedEnv;
