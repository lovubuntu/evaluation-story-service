import { ContainerBuilder } from 'node-dependency-injection';
import 'reflect-metadata';
import { logger } from './logger';

import { boot as bootAPI } from './API/DependencyInjection';
import { boot as bootAuth } from './Components/Auth/DependencyInjection';
import { boot as bootDocs } from './Components/Docs/DependencyInjection';
import { boot as bootStory } from './Components/Story/DependencyInjection';
import { boot as bootUser } from './Components/User/DependencyInjection';
import { Env } from './envSchema';
import { boot as bootInfra } from './Infra/DependencyInjection';

export {types as APITypes} from './API/Types';
export {types as InfraTypes} from './Infra/Types';
export {types as DocTypes} from './Components/Docs/Types';
export {types as UserTypes} from './Components/User/Types';
export {types as AuthTypes} from './Components/Auth/Types';
export {types as StoryTypes} from './Components/Story/Types';

export interface Bootloaders {
  [key: string]: (container: ContainerBuilder, env: Env) => Promise<ContainerBuilder>
}

const containerBootloaders: Bootloaders = {
  bootInfra,
  bootAPI,
  bootDocs,
  bootUser,
  bootAuth,
  bootStory,
};

export async function boot(env: Env): Promise<ContainerBuilder> {
  const container = new ContainerBuilder();

  for (const containerName of Object.keys(containerBootloaders)) {
    logger.info(`Loading ${containerName} container...`);
    await containerBootloaders[containerName](container, env);
  }

  container.compile();

  return container;
}
