import { Ajv } from 'ajv';
import { NextFunction, Request, Response } from 'express';
import { ClientError } from './errors/ClientError';

export class SchemaValidationMiddleware {
  constructor(private validator: Ajv) {
  }

  public execute(schema: any) {
    return (request: Request, response: Response, next: NextFunction) => {
      this.performValidation(request, schema);

      next();
    };
  }

  private performValidation(request: Request, schema: any) {
    [
      {
        schema: schema.params,
        data: request.params,
      },
      {
        schema: schema.body,
        data: request.body,
      },
    ].forEach((validation) => {
      if (validation.schema) {
        const valid = this.validator.validate(validation.schema, validation.data);
        if (!valid) {
          throw new ClientError(`Missing mandatory fields: ${this.validator.errorsText()}`);
        }
      }
    });
  }
}
