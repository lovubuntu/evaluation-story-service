import { Application } from 'express';
import * as asyncHandler from 'express-async-handler';

import { AuthMiddleware } from '../Components/Auth/AuthMiddleware';
import AuthRoutes from '../Components/Auth/AuthRoutes';
import SwaggerRoutes from '../Components/Docs/SwaggerRoutes';
import { StoriesRoutes } from '../Components/Story/StoriesRoutes';
import UsersRoutes from '../Components/User/UsersRoutes';

export class Routes {
  constructor(
    private swaggerRoutes: SwaggerRoutes,
    private authMiddleware: AuthMiddleware,
    private authRoutes: AuthRoutes,
    private userRoutes: UsersRoutes,
    private storyRoutes: StoriesRoutes,
  ) {
  }

  public routes(app: Application): void {
    this.swaggerRoutes.routes(app);
    this.authRoutes.routes(app);
    this.userRoutes.routes(app);

    app.use(asyncHandler(this.authMiddleware.execute.bind(this.authMiddleware)));

    this.storyRoutes.routes(app);
  }
}
