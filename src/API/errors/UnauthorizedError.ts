import { HTTPClientError } from './HTTPClientError';

export class UnauthorizedError extends HTTPClientError {
  public readonly statusCode = 401;

  constructor(message: string | object = 'Unauthorized') {
    super(message);
  }
}
