import { HTTPClientError } from './HTTPClientError';

export class NotFoundError extends HTTPClientError {
  public readonly statusCode = 404;

  constructor(message: string | object = 'Not found') {
    super(message);
  }
}
