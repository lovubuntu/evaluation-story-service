import { HTTPClientError } from './HTTPClientError';

export class ForbiddenError extends HTTPClientError {
  public readonly statusCode = 403;

  constructor(message: string | object = 'Forbidden') {
    super(message);
  }
}
