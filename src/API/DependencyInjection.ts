import * as Ajv from 'ajv';
import * as express from 'express';
import { ContainerBuilder, Reference } from 'node-dependency-injection';
import { Env } from '../envSchema';
import { types as InfraTypes } from '../Infra/Types';

import { AuthTypes, DocTypes, StoryTypes, UserTypes } from '../kernel';
import { ApiErrorHandler } from './ApiErrorHandler';
import { App } from './App';
import { Routes } from './Routes';
import { SchemaValidationMiddleware } from './SchemaValidationMiddleware';
import { types } from './Types';

export async function boot(container: ContainerBuilder, env: Env) {
  const app = express();
  const appDefinition = container.register(types.Express.toString());
  appDefinition.synthetic = true;
  container.set(types.Express.toString(), app);

  container
    .register(types.App.toString(), App)
    .addArgument(new Reference(types.Express.toString()))
    .addArgument(new Reference(types.Routes.toString()))
    .addArgument(new Reference(types.ApiErrorHandler.toString()));

  container
    .register(types.Routes.toString(), Routes)
    .addArgument(new Reference(DocTypes.SwaggerRoutes.toString()))
    .addArgument(new Reference(AuthTypes.AuthMiddleware.toString()))
    .addArgument(new Reference(UserTypes.UsersRoutes.toString()))
    .addArgument(new Reference(AuthTypes.AuthRoutes.toString()))
    .addArgument(new Reference(StoryTypes.StoryRoutes.toString()));

  container
    .register(types.ApiErrorHandler.toString(), ApiErrorHandler)
    .addArgument(new Reference(InfraTypes.Logger.toString()))
    .addArgument(env.infra.NODE_ENV);

  const ajv = Ajv({allErrors: true, removeAdditional: true});

  const schemaValidatorDefinition = container.register(types.SchemaValidator.toString());
  schemaValidatorDefinition.synthetic = true;
  container.set(types.SchemaValidator.toString(), ajv);

  container
    .register(types.SchemaValidationMiddleware.toString(), SchemaValidationMiddleware)
    .addArgument(new Reference(types.SchemaValidator.toString()));

  return container;
}
