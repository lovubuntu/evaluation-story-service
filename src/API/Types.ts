const bundleName = 'API';

export const types = {
  App: Symbol(`${bundleName}.App`),
  ApiErrorHandler: Symbol(`${bundleName}.ApiErrorHandler`),
  OpenAPIHandler: Symbol(`${bundleName}.OpenAPIHandler`),
  Express: Symbol(`${bundleName}.Express`),
  LoginController: Symbol(`${bundleName}.LoginController`),
  TokenController: Symbol(`${bundleName}.TokenController`),
  PasswordController: Symbol(`${bundleName}.PasswordController`),
  HealthCheckController: Symbol(`${bundleName}.HealthCheckController`),
  Routes: Symbol(`${bundleName}.Routes`),
  SchemaValidationMiddleware: Symbol(`${bundleName}.SchemaValidationMiddleware`),
  SchemaValidator: Symbol(`${bundleName}.SchemaValidator`),
  LoginRoutes: Symbol(`${bundleName}.LoginRoutes`),
};
