import { NextFunction, Request, Response } from 'express';
import { Logger } from 'winston';
import { HTTPClientError } from './errors/HTTPClientError';

export class ApiErrorHandler {
  constructor(
    private logger: Logger,
    private appEnvironment: string,
  ) {
  }

  public handle(error: Error, request: Request, response: Response, _next: NextFunction) {
    if (error instanceof HTTPClientError) {
      return this.handleClientError(error, request, response);
    }
    return this.serverError(error, request, response);
  }

  private handleClientError(err: HTTPClientError, _req: Request, res: Response) {
    err.requestId = res.get('X-Request-Id');
    this.logger.warn({name: err.name, requestId: err.requestId, message: err.message, stack: err.stack});
    return res.status(err.statusCode).json({message: err.message});
  }

  private serverError(err: Error, _req: Request, res: Response) {
    this.logger.error({requestId: res.get('X-Request-Id'), message: err.message, stack: err.stack});
    if (this.appEnvironment === 'production') {
      return res.status(500).json({message: 'Internal Server Error'});
    }
    return res.status(500).json({stack: err.stack, message: err.message});
  }

}
