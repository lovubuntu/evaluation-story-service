import {MigrationInterface, QueryRunner} from "typeorm";

export class AddsUserToStory1585716964130 implements MigrationInterface {
  name = 'AddsUserToStory1585716964130';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "story" ADD "user_id" integer`, undefined);
    await queryRunner.query(`ALTER TABLE "story" ADD CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "story" DROP CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c"`, undefined);
    await queryRunner.query(`ALTER TABLE "story" DROP COLUMN "user_id"`, undefined);
  }

}
