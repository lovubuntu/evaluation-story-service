import {MigrationInterface, QueryRunner} from "typeorm";

export class AddsUniqueConstraintToUserEmail1586716187791 implements MigrationInterface {
    name = 'AddsUniqueConstraintToUserEmail1586716187791'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email")`, undefined);
        await queryRunner.query(`ALTER TABLE "story" DROP CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c"`, undefined);
        await queryRunner.query(`ALTER TABLE "story" ALTER COLUMN "user_id" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "story" ADD CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "story" DROP CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c"`, undefined);
        await queryRunner.query(`ALTER TABLE "story" ALTER COLUMN "user_id" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "story" ADD CONSTRAINT "FK_cac14a7871997a849f8fc2dd20c" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22"`, undefined);
    }

}
