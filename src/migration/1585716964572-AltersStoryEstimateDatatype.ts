import {MigrationInterface, QueryRunner} from "typeorm";

export class AddsUserToStory1585716964130 implements MigrationInterface {
  name = 'AltersStoryEstimateDatatype1585716964572';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "story" ALTER COLUMN "estimated_days_to_complete" TYPE numeric(7,2)`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "story" ALTER COLUMN "estimated_days_to_complete" TYPE integer`, undefined);
  }

}
