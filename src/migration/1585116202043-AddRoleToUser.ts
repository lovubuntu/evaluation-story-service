import {MigrationInterface, QueryRunner} from "typeorm";

export class AddRoleToUser1585116202043 implements MigrationInterface {
  name = 'AddRoleToUser1585116202043';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('ADMIN', 'MEMBER')`, undefined);
    await queryRunner.query(`ALTER TABLE "user" ADD "role" "user_role_enum" NOT NULL DEFAULT 'MEMBER'`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "role"`, undefined);
    await queryRunner.query(`DROP TYPE "user_role_enum"`, undefined);
  }

}
