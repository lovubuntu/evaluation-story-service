import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateStoryTable1585631263344 implements MigrationInterface {
  name = 'CreateStoryTable1585631263344';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TYPE "story_type_enum" AS ENUM('ENHANCEMENT', 'BUGFIX', 'DEVELOPMENT', 'QA')`, undefined);
    await queryRunner.query(`CREATE TYPE "story_complexity_enum" AS ENUM('SMALL', 'MEDIUM', 'LARGE', 'EXTRA_LARGE')`, undefined);
    await queryRunner.query(`CREATE TYPE "story_status_enum" AS ENUM('REQUEST_FOR_APPROVAL', 'APPROVED', 'REJECTED')`, undefined);
    await queryRunner.query(`CREATE TABLE "story" ("id" SERIAL NOT NULL, "summary" character varying NOT NULL, "description" character varying NOT NULL, "type" "story_type_enum" NOT NULL, "complexity" "story_complexity_enum" NOT NULL, "estimated_days_to_complete" integer NOT NULL, "cost" integer NOT NULL, "status" "story_status_enum" NOT NULL DEFAULT 'REQUEST_FOR_APPROVAL', CONSTRAINT "PK_28fce6873d61e2cace70a0f3361" PRIMARY KEY ("id"))`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "story"`, undefined);
    await queryRunner.query(`DROP TYPE "story_status_enum"`, undefined);
    await queryRunner.query(`DROP TYPE "story_complexity_enum"`, undefined);
    await queryRunner.query(`DROP TYPE "story_type_enum"`, undefined);
  }

}
