import { sign, verify } from 'jsonwebtoken';
import { LoggerInterface } from '../../Infra/LoggerInterface';
import { User } from '../User/User';
import { UserResponseType } from '../User/UserResponseType';
import { AccessTokenPayload } from './AccessTokenPayload';

export default class TokenManager {
  constructor(
    private jwtSecret: string,
    private logger: LoggerInterface,
  ) {
  }

  public async generateToken(payload: UserResponseType) {
    this.logger.debug('Attempting to generate tokens');

    const jwtUser: { [key: string]: any } = payload;
    const accessToken = await sign({user: jwtUser}, this.jwtSecret);
    this.logger.debug('Tokens generated successfully');

    return accessToken;
  }

  public async authenticateUser(token: string): Promise<Partial<User> | null> {
    return (verify(token, this.jwtSecret) as AccessTokenPayload).user;
  }
}
