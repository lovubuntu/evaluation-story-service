const bundleName = 'User';

export const types = {
  TokenManager: Symbol(`${bundleName}.TokenManager`),
  AuthService: Symbol(`${bundleName}.AuthService`),
  AuthController: Symbol(`${bundleName}.AuthController`),
  AuthMiddleware: Symbol(`${bundleName}.AuthMiddleware`),
  AuthRoutes: Symbol(`${bundleName}.AuthRoutes`),
};
