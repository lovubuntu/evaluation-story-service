import { NextFunction, Request, Response } from 'express';
import { UnauthorizedError } from '../../API/errors/UnauthorizedError';
import TokenManager from './TokenManager';

export class AuthMiddleware {
  constructor(
    private tokenManager: TokenManager,
  ) {
  }

  public async execute(request: Request, response: Response, next: NextFunction) {
    const authorizationHeader = request.headers.authorization as string;

    if (!authorizationHeader) {
      throw new UnauthorizedError('No token provided.');
    }

    let user;
    try {
      user = await this.tokenManager.authenticateUser(authorizationHeader.replace('Bearer ', ''));
    } catch (error) {
      throw new UnauthorizedError(error.message);
    }

    if (!user) {
      throw new UnauthorizedError('User unauthenticated.');
    }

    response.locals.user = user;

    return next();
  }
}
