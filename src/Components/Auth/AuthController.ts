import { Request, Response } from 'express';
import AuthService from './AuthService';

export class AuthController {
  constructor(private authService: AuthService) {
  }

  public async handleLogin(request: Request, response: Response) {
    const {email, password} = request.body;

    const loginResponse = await this.authService.login(email, password);

    return response.json(loginResponse);
  }
}
