import * as bcrypt from 'bcrypt';
import { UnauthorizedError } from '../../API/errors/UnauthorizedError';
import { UserResponseType } from '../User/UserResponseType';
import UsersService from '../User/UsersService';
import { LoginResponseParams } from './LoginResponseParams';
import TokenManager from './TokenManager';

export default class AuthService {
  constructor(
    private userService: UsersService,
    private tokenManager: TokenManager,
  ) {
  }

  public async login(email: string, password: string): Promise<LoginResponseParams> {
    const user = await this.getValidatedUser(email, password);
    const token = await this.tokenManager.generateToken(user);

    return {user, token};
  }

  private async getValidatedUser(email: string, password: string): Promise<UserResponseType> {
    const user = await this.userService.getUserByEmail(email);
    if (!user) {
      throw new UnauthorizedError('Invalid username/password');
    }

    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      throw new UnauthorizedError('Invalid username/password');
    }

    delete user.password;

    return user;
  }
}
