import { ContainerBuilder, Reference } from 'node-dependency-injection';

import { Env } from '../../envSchema';
import { APITypes, InfraTypes, UserTypes } from '../../kernel';
import { AuthController } from './AuthController';
import { AuthMiddleware } from './AuthMiddleware';
import AuthRoutes from './AuthRoutes';
import AuthService from './AuthService';
import TokenManager from './TokenManager';
import { types } from './Types';

export async function boot(container: ContainerBuilder, env: Env) {
  container
    .register(types.TokenManager.toString(), TokenManager)
    .addArgument(env.application.JWT_SECRET)
    .addArgument(new Reference(InfraTypes.Logger.toString()));

  container
    .register(types.AuthService.toString(), AuthService)
    .addArgument(new Reference(UserTypes.UsersService.toString()))
    .addArgument(new Reference(types.TokenManager.toString()));

  container
    .register(types.AuthController.toString(), AuthController)
    .addArgument(new Reference(types.AuthService.toString()));

  container
    .register(types.AuthRoutes.toString(), AuthRoutes)
    .addArgument(new Reference(types.AuthController.toString()))
    .addArgument(new Reference(APITypes.SchemaValidationMiddleware.toString()));

  container
    .register(types.AuthMiddleware.toString(), AuthMiddleware)
    .addArgument(new Reference(types.TokenManager.toString()));

  return container;
}
