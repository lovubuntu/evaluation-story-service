import { User } from '../User/User';

export interface AccessTokenPayload {
  user: Pick<User, Exclude<keyof User, 'password'>>,
}
