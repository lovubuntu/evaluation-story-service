import { Application } from 'express';
import * as asyncHandler from 'express-async-handler';
import { SchemaValidationMiddleware } from '../../API/SchemaValidationMiddleware';
import { userLoginSchema } from './ApiSchema/LoginSchema';
import { AuthController } from './AuthController';

export default class AuthRoutes {
  public constructor(private authController: AuthController,
                     private schemaValidator: SchemaValidationMiddleware) {
  }

  public routes(app: Application): void {
    app.route('/auth/login')
      .post(
        this.schemaValidator.execute(userLoginSchema),
        asyncHandler(this.authController.handleLogin.bind(this.authController)),
      );
  }
}
