import { UserResponseType } from '../User/UserResponseType';

export interface LoginResponseParams {
  token: string;
  user: UserResponseType;
}
