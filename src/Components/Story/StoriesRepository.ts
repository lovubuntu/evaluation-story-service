import { Connection } from 'typeorm';
import { NotFoundError } from '../../API/errors/NotFoundError';
import { CreateStoryParams } from './CreateStoryParams';
import { Story } from './Story';
import { StoryStatus } from './StoryStatus';

export class StoriesRepository {
  public constructor(private connection: Connection) {
  }

  public async createStory(params: CreateStoryParams) {
    const story = this.connection.manager.create(Story, params);
    return this.connection.manager.save(story);
  }

  public async getStoriesByUser(userId: number) {
    return this.connection.manager.find(Story, {where: {userId}});
  }

  public async getAllUserStories() {
    return this.connection.manager.find(Story);
  }

  public async updateStatus(id: number, status: StoryStatus) {
    const result = await this.connection.manager.update(Story, {id}, {status});
    if (result.affected === 0) {
      throw new NotFoundError();
    }
  }
}
