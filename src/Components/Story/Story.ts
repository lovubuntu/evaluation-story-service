import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../User/User';
import { StoryComplexity } from './StoryComplexity';
import { StoryStatus } from './StoryStatus';
import { StoryType } from './StoryType';

@Entity()
export class Story extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public summary: string;

  @Column()
  public description: string;

  @Column({
    type: 'enum',
    enum: StoryType,
  })
  public type: StoryType;

  @Column({
    type: 'enum',
    enum: StoryComplexity,
  })
  public complexity: StoryComplexity;

  @Column('numeric', {
    precision: 7,
    scale: 2,
  })
  public estimatedDaysToComplete: number;

  @Column()
  public cost: number;

  @Column({
    type: 'enum',
    enum: StoryStatus,
    default: StoryStatus.REQUEST_FOR_APPROVAL,
  })
  public status: StoryStatus;

  @Column()
  public userId: number;

  @ManyToOne((type) => User)
  public user: User;
}
