import { StoryComplexity } from '../StoryComplexity';
import { StoryStatus } from '../StoryStatus';
import { StoryType } from '../StoryType';

const baseStoryProperties = {
  summary: {
    type: 'string',
  },
  description: {
    type: 'string',
  },
  type: {
    type: 'string',
    enum: Object.values(StoryType),
  },
  complexity: {
    type: 'string',
    enum: Object.values(StoryComplexity),
  },
  estimatedDaysToComplete: {
    type: 'number',
    multipleOf: 0.25,
    minimum: 0.25,
  },
  cost: {
    type: 'integer',
    minimum: 1,
  },
};

const storyProperties = {
  ...baseStoryProperties,
  status: {
    type: 'string',
    enum: Object.values(StoryStatus),
  },
  id: {
    type: 'string',
  },
};

export const storySchema = {
  body: {
    additionalProperties: false,
    required: Object.keys(baseStoryProperties),
    properties: baseStoryProperties,
  },
  response: {
    additionalProperties: false,
    properties: storyProperties,
  },
};

export const storyReviewSchema = {
  params: {
    additionalProperties: false,
    properties: {
      id: {
        type: 'string',
      },
    },
  },
  body: {
    additionalProperties: false,
    properties: {
      status: {
        type: 'string',
        enum: Object.values(StoryStatus),
      },
    },
    required: ['status'],
  },
  response: {
    additionalProperties: false,
  },
};
