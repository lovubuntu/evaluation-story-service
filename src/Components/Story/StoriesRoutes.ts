import { Application } from 'express';
import * as asyncHandler from 'express-async-handler';
import { SchemaValidationMiddleware } from '../../API/SchemaValidationMiddleware';
import { storyReviewSchema, storySchema } from './ApiSchema/StorySchema';
import { StoriesController } from './StoriesController';

export class StoriesRoutes {
  constructor(private storyController: StoriesController,
              private schemaValidationMiddleware: SchemaValidationMiddleware) {
  }

  public routes(app: Application): void {
    app.route('/stories')
      .post(
        this.schemaValidationMiddleware.execute(storySchema),
        asyncHandler(this.storyController.handleCreate.bind(this.storyController)),
      );

    app.route('/stories')
      .get(
        asyncHandler(this.storyController.getUserStories.bind(this.storyController)),
      );

    app.route('/stories/:id/review')
      .put(
        this.schemaValidationMiddleware.execute(storyReviewSchema),
        asyncHandler(this.storyController.handleReview.bind(this.storyController)),
      );
  }
}
