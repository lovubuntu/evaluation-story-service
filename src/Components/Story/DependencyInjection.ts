import { ContainerBuilder, Reference } from 'node-dependency-injection';

import { Env } from '../../envSchema';
import { APITypes, InfraTypes } from '../../kernel';
import { StoriesController } from './StoriesController';
import { StoriesRepository } from './StoriesRepository';
import { StoriesRoutes } from './StoriesRoutes';
import { StoriesService } from './StoriesService';
import { types } from './Types';

export async function boot(container: ContainerBuilder, _env: Env) {

  container
    .register(types.StoryRepository.toString(), StoriesRepository)
    .addArgument(new Reference(InfraTypes.DbConnection.toString()));

  container
    .register(types.StoryService.toString(), StoriesService)
    .addArgument(new Reference(types.StoryRepository.toString()));

  container
    .register(types.StoryController.toString(), StoriesController)
    .addArgument(new Reference(types.StoryService.toString()));

  container
    .register(types.StoryRoutes.toString(), StoriesRoutes)
    .addArgument(new Reference(types.StoryController.toString()))
    .addArgument(new Reference(APITypes.SchemaValidationMiddleware.toString()));

  return container;
}
