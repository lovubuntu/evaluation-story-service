import { StoryComplexity } from './StoryComplexity';
import { StoryType } from './StoryType';

export interface CreateStoryParams {
  summary: string,
  description: string,
  type: StoryType,
  complexity: StoryComplexity,
  estimatedDaysToComplete: number,
  cost: number,
  userId?: number,
}
