import { ForbiddenError } from '../../API/errors/ForbiddenError';
import { UserResponseType } from '../User/UserResponseType';
import { UserRole } from '../User/UserRole';
import { CreateStoryParams } from './CreateStoryParams';
import { StoriesRepository } from './StoriesRepository';
import { StoryStatus } from './StoryStatus';

export class StoriesService {
  constructor(private storiesRepository: StoriesRepository) {
  }

  public async createUserStory(params: CreateStoryParams, user: UserResponseType) {
    return this.storiesRepository.createStory({...params, userId: user.id});
  }

  public async getStories(user: UserResponseType) {
    if (user.role === UserRole.ADMIN) {
      return this.storiesRepository.getAllUserStories();
    }
    return this.storiesRepository.getStoriesByUser(user.id);
  }

  public async review(storyId: number, status: StoryStatus, user: UserResponseType) {
    if (user.role !== UserRole.ADMIN) {
      throw new ForbiddenError('You dont have enough permission to review');
    }

    return this.storiesRepository.updateStatus(storyId, status);
  }
}
