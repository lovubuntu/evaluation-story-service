import { Request, Response } from 'express';
import { CreateStoryParams } from './CreateStoryParams';
import { StoriesService } from './StoriesService';

export class StoriesController {
  constructor(private storiesService: StoriesService) {
  }

  public async handleCreate(request: Request, response: Response) {
    const storyParams: CreateStoryParams = {
      summary: request.body.summary,
      description: request.body.description,
      type: request.body.type,
      complexity: request.body.complexity,
      estimatedDaysToComplete: request.body.estimatedDaysToComplete,
      cost: request.body.cost,
    };
    const user = response.locals.user;

    const story = await this.storiesService.createUserStory(storyParams, user);

    return response.status(201).json(story);
  }

  public async getUserStories(_request: Request, response: Response) {
    const user = response.locals.user;
    const stories = await this.storiesService.getStories(user);

    return response.json({stories});
  }

  public async handleReview(request: Request, response: Response) {
    const storyId = Number.parseInt(request.params.id, 10);
    const status = request.body.status;
    await this.storiesService.review(storyId, status, response.locals.user);

    return response.status(204).json();
  }
}
