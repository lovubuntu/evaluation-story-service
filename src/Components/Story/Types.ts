const bundleName = 'User';

export const types = {
  StoryService: Symbol(`${bundleName}.StoryService`),
  StoryRepository: Symbol(`${bundleName}.StoryRepository`),
  StoryController: Symbol(`${bundleName}.StoryController`),
  StoryMiddleware: Symbol(`${bundleName}.StoryMiddleware`),
  StoryRoutes: Symbol(`${bundleName}.StoryRoutes`),
};
