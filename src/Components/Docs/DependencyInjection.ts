import { ContainerBuilder } from 'node-dependency-injection';

import { Env } from '../../envSchema';
import SwaggerRoutes from './SwaggerRoutes';
import { types } from './Types';

export async function boot(container: ContainerBuilder, env: Env) {
  container
    .register(types.SwaggerRoutes.toString(), SwaggerRoutes);
  return container;
}
