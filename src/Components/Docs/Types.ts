const bundleName = 'Docs';

export const types = {
  SwaggerRoutes: Symbol(`${bundleName}.SwaggerRoutes`),
};
