import { Application } from 'express';
import * as swaggerUi from 'swagger-ui-express';
import * as YAML from 'yamljs';

export default class SwaggerRoutes {

  public routes(app: Application): void {
    const swaggerDoc = YAML.load(`${__dirname}/../../../openapi.yml`);
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
  }
}
