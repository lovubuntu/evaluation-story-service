import { Connection } from 'typeorm';
import { User } from './User';
import { UserCreateRequestParams } from './UserCreateRequestParams';

export default class UsersRepository {
  public constructor(
    private connection: Connection,
  ) {
  }

  public async createUser(params: UserCreateRequestParams) {
    const user = this.connection.manager.create(User, params);

    return this.connection.manager.save(user);
  }

  public async getUserByEmail(email: string): Promise<User | undefined> {
    return this.connection.manager.findOne(User, {email});
  }
}
