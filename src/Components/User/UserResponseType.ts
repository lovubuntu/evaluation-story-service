import { UserRole } from './UserRole';

export interface UserResponseType {
  id: number;
  name: string;
  email: string;
  role: UserRole;
}
