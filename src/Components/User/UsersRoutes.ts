import { Application } from 'express';
import * as asyncHandler from 'express-async-handler';
import UsersController from './UsersController';

export default class UsersRoutes {
  public constructor(private usersController: UsersController) {
  }

  public routes(app: Application): void {
    app.route('/users')
      .post(
        asyncHandler(this.usersController.create.bind(this.usersController)),
      );
  }
}
