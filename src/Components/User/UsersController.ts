import { Request, Response } from 'express';
import { UserCreateRequestParams } from './UserCreateRequestParams';
import UsersService from './UsersService';

export default class UsersController {
  constructor(private usersService: UsersService) {
  }

  public async create(request: Request, response: Response) {
    const userParams: UserCreateRequestParams = {
      name: request.body.name,
      email: request.body.email,
      password: request.body.password,
      role: request.body.role,
    };

    const user = await this.usersService.saveUser(userParams);

    delete user.password;

    return response.json({user});
  }

}
