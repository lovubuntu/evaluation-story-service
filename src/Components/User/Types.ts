const bundleName = 'User';

export const types = {
  UsersRepository: Symbol(`${bundleName}.UsersRepository`),
  UsersService: Symbol(`${bundleName}.UsersService`),
  UsersController: Symbol(`${bundleName}.UsersController`),
  UsersRoutes: Symbol(`${bundleName}.UsersRoutes`),
};
