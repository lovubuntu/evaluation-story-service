import { UserRole } from './UserRole';

export interface UserCreateRequestParams {
  name: string;
  email: string;
  password: string;
  role: UserRole;
}
