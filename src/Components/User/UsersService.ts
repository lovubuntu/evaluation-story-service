import * as bcrypt from 'bcrypt';
import { User } from './User';
import { UserCreateRequestParams } from './UserCreateRequestParams';
import UsersRepository from './UsersRepository';

export default class UsersService {
  constructor(private userRepository: UsersRepository) {
  }

  public async saveUser(params: UserCreateRequestParams) {
    return await this.userRepository.createUser({
      name: params.name,
      email: params.email,
      password: await bcrypt.hash(params.password, 10),
      role: params.role,
    });
  }

  public async getUserByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.getUserByEmail(email);
  }
}
