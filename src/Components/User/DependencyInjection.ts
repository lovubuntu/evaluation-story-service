import { ContainerBuilder, Reference } from 'node-dependency-injection';

import { Env } from '../../envSchema';
import { InfraTypes } from '../../kernel';
import { types } from './Types';
import UsersController from './UsersController';
import UsersRepository from './UsersRepository';
import UsersRoutes from './UsersRoutes';
import UsersService from './UsersService';

export async function boot(container: ContainerBuilder, _env: Env) {
  container
    .register(types.UsersRepository.toString(), UsersRepository)
    .addArgument(new Reference(InfraTypes.DbConnection.toString()));

  container
    .register(types.UsersService.toString(), UsersService)
    .addArgument(new Reference(types.UsersRepository.toString()));

  container
    .register(types.UsersController.toString(), UsersController)
    .addArgument(new Reference(types.UsersService.toString()));

  container
    .register(types.UsersRoutes.toString(), UsersRoutes)
    .addArgument(new Reference(types.UsersController.toString()));

  return container;
}
