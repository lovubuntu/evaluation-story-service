import { envGroup, envSchema, TypeOf, types } from '@freighthub/typed-env';

const application = envGroup({
  JWT_SECRET: types.NonEmptyString,
});

const infra = envGroup({
  PORT: types.PortNumber,
  LOG_LEVEL: types.UnionOf(['error', 'warn', 'info', 'verbose', 'debug']),
  NODE_ENV: types.UnionOf(['production', 'development', 'sandbox', 'test']),
});

const db = envGroup({
  HOST: types.NonEmptyString,
  PORT: types.PortNumber,
  USERNAME: types.NonEmptyString,
  PASSWORD: types.OptionalString,
  DATABASE: types.NonEmptyString,
  LOGGING: types.Boolean,
}, 'DB');

export const schema = envSchema({
  application,
  infra,
  db,
});

export type Env = TypeOf<typeof schema>;
