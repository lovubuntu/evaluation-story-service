import { ContainerBuilder } from 'node-dependency-injection';
import { logger } from '../logger';

import { createConnection, getConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { Env } from '../envSchema';
import { types } from './Types';

export async function boot(container: ContainerBuilder, env: Env) {
  const connectionOptions = await getConnectionOptions();
  Object.assign(connectionOptions, {
    namingStrategy: new SnakeNamingStrategy(),
    port: env.db.PORT,
    host: env.db.HOST,
    username: env.db.USERNAME,
    password: env.db.PASSWORD,
    database: env.db.DATABASE,
    logging: env.db.LOGGING === 'true',
  });
  const dbConnection = await createConnection(connectionOptions);

  await dbConnection.runMigrations();

  const dbConnectionDefinition = container.register(types.DbConnection.toString());
  dbConnectionDefinition.synthetic = true;
  container.set(types.DbConnection.toString(), dbConnection);

  const loggerDefinition = container.register(types.Logger.toString());
  loggerDefinition.synthetic = true;
  container.set(types.Logger.toString(), logger);

  return container;
}
