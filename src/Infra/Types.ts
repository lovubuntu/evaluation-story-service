const bundleName = 'Infra';

export const types = {
  DbConnection: Symbol(`${bundleName}.DbConnection`),
  Logger: Symbol(`${bundleName}.Logger`),
};
